﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using SQLite;
using System.Reflection;
using IzdajaRacunovWPF.Data.Entity;
using IzdajaRacunovWPF.Utilities;
using System.Collections.ObjectModel;

namespace IzdajaRacunovWPF.Data
{
    public class DataService
    {
        /// <summary>
        /// We initialize the database service class
        /// </summary>
        public void Initialize()
        {
            try
            {
                AppData.Logger.Info("Initializing data service");

                var dbNamespace = typeof(Entity.Person).Namespace;
                var assembly = typeof(Entity.Person).Assembly;
                var types = assembly.GetExportedTypes().Where(t => t.Namespace == dbNamespace && !t.IsAbstract).ToArray();

                foreach (Type type in types)
                {
                    AppData.DBObject.CreateTable(type);
                }

            }catch(Exception e){
                AppData.Logger.Error(e);
            }
        }

        public void SaveData(ObservableCollection<ServiceItemType> types, List<ServiceItemType> toBeDeleted, out Boolean existing)
        {
            existing = false;

            AppData.Logger.Info("Saving material data");

            try
            {
                List<ServiceItem> withConstraints;

                foreach (ServiceItemType item in toBeDeleted)
                {
                    withConstraints = AppData.DBObject.Query<ServiceItem>(@"select * from ServiceItem where TypeId = ?", item.Id);
                    if (withConstraints.Count == 0)
                        AppData.DBObject.Delete(item);
                    else
                        existing = true;
                }

                foreach (ServiceItemType item in types)
                {
                    if (item.Id < 1)
                        AppData.DBObject.Insert(item);
                    else
                        AppData.DBObject.Update(item);
                }

            }
            catch (Exception e)
            {
                AppData.Logger.Error(e);
            }
        }

        /// <summary>
        /// Handles data inserts/updates for a billing form
        /// </summary>
        /// <param name="Bill"></param>
        public void SaveData(BillingForm Bill)
        {
            try
            {
                AppData.Logger.Info("Saving bill data");

                if (Bill.Id < 1)
                    AppData.DBObject.Insert(Bill);

                if (Bill.Customer.Id < 1 && !String.IsNullOrEmpty(Bill.Customer.Title))
                    AppData.DBObject.Insert(Bill.Customer);
                else if(Bill.Customer.Id > 0)
                    AppData.DBObject.Update(Bill.Customer);

                if (Bill.Provider != null && Bill.Provider.Id < 1 && !String.IsNullOrEmpty(Bill.Provider.Title))
                    AppData.DBObject.Insert(Bill.Provider);
                else if(Bill.Provider != null && Bill.Provider.Id > 0)
                    AppData.DBObject.Update(Bill.Provider);

                Bill.CustomerId = Bill.Customer.Id;

                if(Bill.Provider != null)
                    Bill.ProviderId = Bill.Provider.Id;
                
                foreach (ServiceItem item in Bill.ServiceItems)
                {
                    if (item.Type != null && item.Type.Id < 1 && !String.IsNullOrEmpty(item.Type.Title))
                        AppData.DBObject.Insert(item.Type);
                    else if (item.Type != null && item.Type.Id > 0)
                        AppData.DBObject.Update(item.Type);

                    item.TypeId = item.Type.Id;
                    item.BillId = Bill.Id;

                    if (item.Id < 1)
                        AppData.DBObject.Insert(item);
                    else
                        AppData.DBObject.Update(item);
                }

                AppData.DBObject.Update(Bill);

                AppData.Logger.Info(String.Format("Saving/updating bill no. - {0}", Bill.Code));
            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show("Shranjevanje v bazo ni uspelo.. :(","Ups..");
                AppData.Logger.Error(e);
            }
        }
    }
}
