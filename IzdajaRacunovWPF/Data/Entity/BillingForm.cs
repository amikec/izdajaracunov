﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQLite;
using System.ComponentModel;
using System.Collections.ObjectModel;
using IzdajaRacunovWPF.Controls.AutoComplete.Interfaces;

namespace IzdajaRacunovWPF.Data.Entity
{
    public class BillingForm : IViewItem
    {
        public BillingForm()
        {
            ServiceItems = new ObservableCollection<ServiceItem>();
            ServiceItems.CollectionChanged += ServiceItems_CollectionChanged;
            ServiceItems.Add(new ServiceItem());

            this.Customer = new Person();
            this.Provider = new Person();
        }

        public String Code { get { return String.Format("100GSI-{0}", this.Datum.Ticks); } }

        [Ignore]
        private Boolean informational;
        public Boolean Informational
        {
            get { return this.informational; }
            set
            {
                this.informational = value;
                OnPropertyChanged("Informational");
            }
        }

        [PrimaryKey, AutoIncrement]
        public override int Id
        {
            get
            {
                return base.Id;
            }
            set
            {
                base.Id = value;
            }
        }

        [Ignore]
        private Person customer;
        [Ignore]
        public Person Customer
        {
            get { return this.customer; }
            set
            {
                this.customer = value;

                if (value != null)
                    this.CustomerId = value.Id;
                OnPropertyChanged("Customer");
            }
        }

        public override string ToString()
        {
            return String.Format("{0} - {1} - {2}", customer.Title, customer.Address, this.datum);
        }

        [Ignore]
        private Person provider;
        [Ignore]
        public Person Provider
        {
            get
            {
                return this.provider;
            }

            set
            {
                this.provider = value;
                if (value != null)
                    ProviderId = value.Id;

                OnPropertyChanged("Provider");
            }
        }

        public int CustomerId
        {
            get;
            set;
        }

        public int ProviderId
        {
            get;
            set;
        }

        [Ignore]
        private Decimal price;
        public Decimal Price
        {
            get
            {
                return this.price;
            }
            set
            {
                this.price = value;
                OnPropertyChanged("Price");
            }
        }

        [Ignore]
        private DateTime datum;
        public DateTime Datum
        {
            get
            {
                return this.datum;
            }
            set
            {
                this.datum = value;
            }
        }

        public Boolean IsClosable()
        {
            if (this.Id < 1 && this.ServiceItems.Count > 0)
                return false;

            return true;
        }

        [Ignore]
        public ObservableCollection<ServiceItem> ServiceItems { get; set; }

        public void AddPostavkaStoritve(ServiceItem postavka)
        {
            this.ServiceItems.Add(postavka);
        }

        public ServiceItem AddPostavkaStoritve(String title, Decimal cost, double quantity)
        {
            ServiceItem item = new ServiceItem();
            item.Title = title;
            item.Price = cost;

            this.ServiceItems.Add(item);

            return item;
        }

        void ServiceItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            ObservableCollection<ServiceItem> items = sender as ObservableCollection<ServiceItem>;
            for (int i = 0; i < items.Count; i++)
            {
                items[i].ItemNo = i + 1;
            }
        }
    }
}
