﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using System.ComponentModel;
using IzdajaRacunovWPF.Controls.AutoComplete.Interfaces;

namespace IzdajaRacunovWPF.Data.Entity
{
    public class Person : IViewItem
    {
        public Person()
        {
            this.Id = -1;
        }

        public override string Title
        {
            get
            {
                return base.Title;
            }
            set
            {
                base.Title = value;
                OnPropertyChanged("Title");
            }
        }

        
        [PrimaryKey, AutoIncrement]
        public override int Id
        {
            get
            {
                return base.Id;
            }
            set
            {
                base.Id = value;
            }
        }

        [Ignore]
        private String email;
        public String Email
        {
            get { return email; }
            set
            {
                this.email = value;
                OnPropertyChanged("Email");
            }
        }

        [Ignore]
        private String tel1;
        public String Tel1
        {
            get { return this.tel1; }
            set
            {
                this.tel1 = value;
                OnPropertyChanged("Tel1");
            }
        }

        public String Tel2 { get; set; }
        public String CitizenNo { get; set; }
        public int TaxNo { get; set; }

        [Ignore]
        private String address;
        public String Address
        {
            get { return this.address; }
            set
            {
                this.address = value;
                OnPropertyChanged("Address");
            }
        }

        [Ignore]
        public String postalCode;
        public String PostalCode
        {
            get { return this.postalCode; }
            set
            {
                this.postalCode = value;
                OnPropertyChanged("PostalCode");
            }
        }

        public override string ToString()
        {
            return this.Title == null ? "" : this.Title;
        }
    }
}
