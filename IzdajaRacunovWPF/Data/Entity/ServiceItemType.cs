﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQLite;
using IzdajaRacunovWPF.Controls.AutoComplete.Interfaces;

namespace IzdajaRacunovWPF.Data.Entity
{
    public class ServiceItemType : IViewItem
    {
        public ServiceItemType()
        {
            this.Id = -1;
        }

        [PrimaryKey, AutoIncrement]
        public override int Id
        {
            get
            {
                return base.Id;
            }
            set
            {
                base.Id = value;
            }
        }

        public override string Title
        {
            get
            {
                if (string.IsNullOrEmpty(base.Title))
                    return "";

                return base.Title;
            }
            set
            {
                base.Title = value;
            }
        }

        [Ignore]
        public String PricePerUnit
        {
            get
            {
                return String.Format("{0} €/{1}", Price, Unit);
            }
        }

        [Ignore]
        private Decimal price;
        public Decimal Price { 
            
            get { return this.price; }
            set
            {
                this.price = value;
                OnPropertyChanged("Price");
            }
        }

        [Ignore]
        private String unit;
        public String Unit
        {
            get
            { return unit; }
            set
            {
                unit = value;
                OnPropertyChanged("Unit");
            }
        }
        
        public override string ToString()
        {
            if (String.IsNullOrEmpty(this.Title))
                return "";
            else
                return this.Title.ToString();
        }
    }
}
