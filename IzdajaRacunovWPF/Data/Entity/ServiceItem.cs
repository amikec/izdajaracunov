﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQLite;
using IzdajaRacunovWPF.Controls.AutoComplete.Interfaces;

namespace IzdajaRacunovWPF.Data.Entity
{
    public class ServiceItem : IViewItem
    {
        public ServiceItem()
        {
            this.Quantity = 1;
            this.Type = new ServiceItemType();
        }

        [AutoIncrement, PrimaryKey]
        public override int Id
        {
            get
            {
                return base.Id;
            }
            set
            {
                base.Id = value;
            }
        }

        public int BillId { get; set; }

        public override string Title
        {
            get
            {
                if (String.IsNullOrEmpty(base.Title))
                    return "";

                return base.Title;
            }
            set
            {
                base.Title = value;
                OnPropertyChanged("Title");
            }
        }

        public Int32 ItemNo { get; set; }

        [Ignore]
        private Decimal price;
        public Decimal Price
        {
            get
            {
                return this.price;
            }

            set
            {
                this.price = value;
                this.Type.Price = value;
                OnPropertyChanged("Price");
            }
        }

        public Decimal Quantity { get; set; }

        [Ignore]
        private int typeId;
        public int TypeId { get { return this.typeId; } set { this.typeId = value; } }

        [Ignore]
        private String unit;
        public String Unit
        {
            get { return this.unit; }
            set
            {
                this.unit = value;
                this.Type.Unit = value;
                OnPropertyChanged("Unit");
            }
        }

        [Ignore]
        private ServiceItemType itemType;
        [Ignore]
        public ServiceItemType Type
        {
            get { return this.itemType; }
            set
            {
                this.itemType = value;
                if(value != null)
                    this.typeId = value.Id;
                OnPropertyChanged("Type");
            }
        }
    }
}
