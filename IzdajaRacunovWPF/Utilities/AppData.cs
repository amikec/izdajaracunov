﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IzdajaRacunovWPF.Data;
using IzdajaRacunovWPF.Data.Entity;
using IzdajaRacunovWPF.ViewModel;
using System.ComponentModel;
using System.Collections.ObjectModel;
using SQLite;
using System.Windows;
using System.Windows.Media;
using System.IO;
using IzdajaRacunovWPF.Utilities.Logging;

namespace IzdajaRacunovWPF.Utilities
{
    public class AppData
    {
        public static ILogger Logger { get; set; }

        public static void Init()
        {
            LocalDataService = new DataService();
            LocalDataService.Initialize();
        }

        public static void RefreshBillsData()
        {
            try
            {
                AppData.Logger.Info("Refresh bills");
                
                Bills.Clear();
                List<BillingForm> bills = DBObject.Query<BillingForm>("SELECT distinct * FROM BillingForm");

                foreach (BillingForm bill in bills)
                {
                    bill.Customer = DBObject.Find<Person>(bill.CustomerId);
                    bill.Provider = DBObject.Find<Person>(bill.ProviderId);

                    List<ServiceItem> items = DBObject.Query<ServiceItem>("select distinct * from ServiceItem where BillId = " + bill.Id);

                    bill.ServiceItems.Clear();

                    foreach (ServiceItem item in items)
                    {
                        item.Type = DBObject.Find<ServiceItemType>(item.TypeId);
                        bill.ServiceItems.Add(item);
                    }

                    Bills.Add(bill);
                }
            }
            catch (Exception e)
            {
                AppData.Logger.Error(e);
            }
        }

        /// <summary>
        /// Refreshes ItemTypes
        /// </summary>
        /// <returns></returns>
        public static ObservableCollection<ServiceItemType> RefreshItemTypes()
        {
            ObservableCollection<ServiceItemType> types = new ObservableCollection<ServiceItemType>();

            try
            {
                AppData.Logger.Info("Refreshing item types");
                List<ServiceItemType> itemTypes = DBObject.Query<ServiceItemType>("SELECT distinct * FROM ServiceItemType");

                foreach (ServiceItemType item in itemTypes)
                    types.Add(item);
            
            }
            catch (Exception e)
            {
                AppData.Logger.Error(e);
            }

            return types;
        }

        public static void RefreshData()
        {
            try
            {
                AppData.Logger.Info("Refreshing data");

                People.Clear();
                ItemTypes.Clear();
                Bills.Clear();

                List<ServiceItemType> itemTypes = DBObject.Query<ServiceItemType>("SELECT distinct * FROM ServiceItemType");

                foreach (ServiceItemType item in itemTypes)
                    ItemTypes.Add(item);

                List<Person> people = DBObject.Query<Person>("SELECT distinct * FROM Person");

                foreach (Person person in people)
                    People.Add(person);

            }
            catch(Exception e)
            {
                AppData.Logger.Error(e);
            }
        }

        public static DataService LocalDataService { get; set; }
        public static ObservableCollection<BillingForm> Bills = new ObservableCollection<BillingForm>();
        public static ObservableCollection<Person> People = new ObservableCollection<Person>();
        public static ObservableCollection<ServiceItemType> ItemTypes = new ObservableCollection<ServiceItemType>();

        private static SQLiteConnection db = null;
        public static SQLiteConnection DBObject
        {
            get
            {
                if (db == null)
                    db = new SQLiteConnection("lib\\database.db");

                return db;
            }
        }

        /// <summary>
        /// Finds a Child of a given item in the visual tree. 
        /// </summary>
        /// <param name="parent">A direct parent of the queried item.</param>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="childName">x:Name or Name of child. </param>
        /// <returns>The first parent item that matches the submitted type parameter. 
        /// If not matching item can be found, 
        /// a null parent is being returned.</returns>
        public static T FindChild<T>(DependencyObject parent, string childName)
           where T : DependencyObject
        {
            // Confirm parent and childName are valid. 
            if (parent == null) return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                // If the child is not of the request child type child
                T childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree
                    foundChild = FindChild<T>(child, childName);

                    // If the child is found, break so we do not overwrite the found child. 
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        // if the child's name is of the request name
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child element found.
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }

        public class CustomLogger
        {
            public CustomLogger()
            {
            }

            public void Log(String msg, Boolean exception)
            {
                String type = exception ? "ERROR" : "TRACE";
                try
                {
                    System.IO.StreamWriter file = new System.IO.StreamWriter("izdaja_racunov.log", true);
                    file.WriteLine("{0} - {1} - {2}", System.DateTime.Now, type, msg);
                    file.Close();
                }
                catch(Exception e)
                {

                }
            }
        }
    }
}
