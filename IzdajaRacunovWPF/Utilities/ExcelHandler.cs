﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using IzdajaRacunovWPF.Data.Entity;
using Microsoft.Office.Interop.Excel;

namespace IzdajaRacunovWPF.Utilities
{
    public class ExcelHandler
    {
        private Application app = null;
        private Workbook workbook = null;
        private Worksheet worksheet = null;
        private Range workSheet_range = null;

        private List<String> cols = new List<string>() {"", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N" };

        public ExcelHandler()
        {
            CreateDoc();
        }

        public void CreateDoc()
        {
            try
            {       
                app = new Application();
                app.Visible = true;
                workbook = app.Workbooks.Add(1);
                worksheet = (Worksheet)workbook.Sheets[1];
            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show(e.Message, "Ooops. Napakica pri kreiranju Excel dokumenta..");
            }
        }

        public void WriteData(BillingForm billingForm)
        {
            try
            {
                String tip = billingForm.Informational ? "Predračun" : "Račun";

                this.Write(String.Format("{0} št. {1}", tip, billingForm.Code), 2, 2, 5, true, false);
                this.Write("Datum: " + billingForm.Datum.ToLocalTime(), 3, 2, 3, false, false);

                int stolpecStranka = 2;

                if (billingForm.Provider != null && billingForm.Provider.Id > 0)
                {
                    stolpecStranka = 7;
                    this.Write("Ponudnik", 5, 2, 2, true, false);
                    this.Write(billingForm.Provider.Title, 6, 2, 1, false, false);
                    this.Write(billingForm.Provider.Address, 7, 2, 1, false, false);
                    this.Write(billingForm.Provider.PostalCode, 8, 2, 1, false, false);
                    this.Write(billingForm.Provider.Email, 9, 2, 1, false, false);
                    this.Write(billingForm.Provider.Tel1, 10, 2, 1, false, false);
                }

                this.Write("Stranka", 5, stolpecStranka, 2, true, false);
                this.Write(billingForm.Customer.Title, 6, stolpecStranka, 1, false, false);
                this.Write(billingForm.Customer.Address, 7, stolpecStranka, 1, false, false);
                this.Write(billingForm.Customer.PostalCode, 8, stolpecStranka, 1, false, false);
                this.Write(billingForm.Customer.Email, 9, stolpecStranka, 1, false, false);
                this.Write(billingForm.Customer.Tel1, 10, stolpecStranka, 1, false, false);
                
                this.Write("Zap. št", 12, 2, 1, true, false);
                this.Write("Storitev", 12, 3, 4, true, false);
                this.Write("Enota", 12, 7, 1, true, false);
                this.Write("Cena (€)", 12, 8, 1, true, false);
                this.Write("Količina", 12, 9, 1, true, false);

                int i = 13;

                foreach (ServiceItem item in billingForm.ServiceItems)
                {
                    this.Write(item.ItemNo, i, 2, 1, false, true);
                    this.Write(item.Type.Title, i, 3, 4, false, true);
                    this.Write(item.Unit, i, 7, 1, false, true);
                    this.Write(item.Price, i, 8, 1, false, true);
                    this.Write(item.Quantity, i, 9, 1, false, true);
                    i++;
                }

                this.Write("Za plačilo (€):", i, 7, 1, true, false);
                this.Write(String.Format("{0} €",billingForm.Price), i, 9, 1, true, false);

                String title = String.Format("Racun-{0}{1}-{2}", billingForm.Customer.Title, billingForm.Datum.ToString("dd-MM-yyyy"), billingForm.Id);
                String path = Path.Combine(Path.Combine(Environment.CurrentDirectory, "Racuni\\"), title);

                if (!Directory.Exists(Path.Combine(Environment.CurrentDirectory, "Racuni")))
                    Directory.CreateDirectory("Racuni");

                this.workbook.SaveAs(path, Type.Missing, Type.Missing, Type.Missing,
            Type.Missing, Type.Missing, XlSaveAsAccessMode.xlNoChange,
            Type.Missing, Type.Missing, Type.Missing, Type.Missing,
            Type.Missing);

            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show("Zapisovanje v Excel ni uspelo ... :/ ", "Ups..");
            }
        }

        private void Write(object data, int row, int col, int span, Boolean bold, Boolean border)
        {
            if (data is String)
                data = data.ToString().Replace(",", ".");

            worksheet.Cells[row, col] = data;
            workSheet_range = worksheet.get_Range(String.Format("{0}{1}", cols[col], row), String.Format("{0}{1}", cols[col + span - 1], row));
            workSheet_range.Merge();
            workSheet_range.Font.Bold = bold;

            if(border)
                workSheet_range.Borders.Color = System.Drawing.Color.Black.ToArgb();
        }
    }
}
