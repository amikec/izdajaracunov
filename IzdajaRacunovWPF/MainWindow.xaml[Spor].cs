﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;

namespace IzdajaRacunovWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public DataSet DataSet { get; set; }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void optionsMenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void newMenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void exitMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (this.DataSet.HasChanges())
            {
                MessageBoxResult result = MessageBox.Show("Vsi podatki niso shranjeni, ali želite vseeno zapustiti aplikacijo?", "Neshranjeni podatki", MessageBoxButton.YesNo);
                
                if(result == MessageBoxResult.Yes)

            }

            this.Close();
        }

        private void Save()
        {

        }
    }
}
