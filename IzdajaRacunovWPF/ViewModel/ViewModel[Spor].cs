﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using IzdajaRacunovWPF.Data;
using IzdajaRacunovWPF.Data.Entity;

namespace IzdajaRacunovWPF.ViewModel
{
    public class ViewModel : INotifyPropertyChanged
    {
        private bool isProcessing;
        public BillingForm Bill;

        public ViewModel()
        {
            this.Bill = new BillingForm();
            DataService localDataService = new DataService();
            localDataService.Initialize();
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
