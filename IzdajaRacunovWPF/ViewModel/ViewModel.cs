﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using IzdajaRacunovWPF.Data;
using IzdajaRacunovWPF.Data.Entity;
using IzdajaRacunovWPF.Utilities;
using IzdajaRacunovWPF.Controls.AutoComplete.Interfaces;

namespace IzdajaRacunovWPF.ViewModel
{
    public class ViewModel : INotifyPropertyChanged
    {
        private BillingForm bill;
        private Boolean isProcessing;
        public BillingForm Bill { get { return bill; } set { this.bill = value; } }
        
        public ObservableCollection<ServiceItemType> ItemTypes { get { return AppData.ItemTypes; } }
        public ObservableCollection<Person> People { get { return AppData.People; } }

        public ViewModel()
        {
            this.Bill = new BillingForm();
        }

        public List<IViewItem> GetPersonBySearchToken(string searchToken, out int count)
        {
            if (!isProcessing)
            {
                isProcessing = true;

                var result = this.People.AsEnumerable().Where(n => n.Title.Split(" ".ToCharArray()).Any(m => m.Length > 0 && searchToken.ToLower().Split(" ".ToCharArray()).Any(k => k.Length > 0 && m.ToLower().StartsWith(k)))).ToList<IViewItem>();
                count = result.Count();

                isProcessing = false;

                return result;
            }

            count = 0;
            return null;
        }

        public List<IViewItem> GetItemTypeBySearchToken(string searchToken, out int count)
        {
            if (!isProcessing)
            {
                isProcessing = true;

                var result = this.ItemTypes.AsEnumerable().Where(n => n.Title.Split(" ".ToCharArray()).Any(m => m.Length > 0 && searchToken.ToLower().Split(" ".ToCharArray()).Any(k => k.Length > 0 && m.ToLower().StartsWith(k)))).ToList<IViewItem>();
                count = result.Count();

                isProcessing = false;

                return result;
            }

            count = 0;
            return null;
        }

        public void Save()
        {
            if(this.Bill.Datum == System.DateTime.MinValue)
                this.Bill.Datum = System.DateTime.Now;

            if (!CheckData())
                return;

            AppData.LocalDataService.SaveData(Bill);

            ExcelHandler excl = new ExcelHandler();
            excl.WriteData(Bill);

            AppData.RefreshData();
        }

        private Boolean CheckData()
        {
            if (String.IsNullOrEmpty(this.Bill.Customer.Title))
            {
                System.Windows.MessageBox.Show("Polje naziv osebe ne sme biti prazno");
                return false;
            }

            foreach (ServiceItem item in Bill.ServiceItems)
            {
                if (String.IsNullOrEmpty(item.Type.Title))
                {
                    System.Windows.MessageBox.Show("Polje 'storitev' mora biti izpolnjeno pri vseh postavkah.");
                    return false;
                }

                if (item.Price == 0 || item.Quantity == 0)
                {
                    System.Windows.MessageBox.Show("Polja 'cena' in 'količina' morajo biti večja od 0.");
                    return false;
                }
            }

            return true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
