﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using IzdajaRacunovWPF.Controls.AutoComplete.Interfaces;
using IzdajaRacunovWPF.Data.Entity;
using IzdajaRacunovWPF.Utilities;

namespace IzdajaRacunovWPF.ViewModel
{
    public class ViewModelMaterial : INotifyPropertyChanged
    {
        private Boolean isProcessing;
        public ObservableCollection<ServiceItemType> ItemTypes { get; set; }
        public ObservableCollection<ServiceItemType> CurrentItemTypes { get; set; }
        public List<ServiceItemType> ToBeDeleted { get; set; }

        public ViewModelMaterial()
        {
            ItemTypes = AppData.RefreshItemTypes();
            CurrentItemTypes = new ObservableCollection<ServiceItemType>();
            ToBeDeleted = new List<ServiceItemType>();
        }

        public List<IViewItem> GetItemTypeBySearchToken(string searchToken, out int count)
        {
            if (!isProcessing)
            {
                isProcessing = true;

                var result = this.ItemTypes.AsEnumerable().Where(n => n.Title.Split(" ".ToCharArray()).Any(m => m.Length > 0 && searchToken.ToLower().Split(" ".ToCharArray()).Any(k => k.Length > 0 && m.ToLower().StartsWith(k)))).ToList<IViewItem>();
                count = result.Count();

                isProcessing = false;

                return result;
            }

            count = 0;
            return null;
        }

        public void Save()
        {
            foreach (ServiceItemType type in CurrentItemTypes)
            {
                if (String.IsNullOrEmpty(type.Title))
                {
                    System.Windows.MessageBox.Show("Vsi materiali morajo imeti izpolnjeno polje 'Naziv'", "Obvestilo");
                    return;
                }
            }

            Boolean constraints;
            AppData.LocalDataService.SaveData(CurrentItemTypes, ToBeDeleted, out constraints);

            if (constraints)
            {
                System.Windows.MessageBox.Show("Spremembe so bile shranjene, vendar nekaterih postavk ni bilo možno izbrisati, ker so že uporabljene v obstoječih računih.", "Obvestilo");
            }
            else
            {
                System.Windows.MessageBox.Show("Spremembe so bile shranjene.");
            }

            ItemTypes = AppData.RefreshItemTypes();
            AppData.RefreshData();
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

}
