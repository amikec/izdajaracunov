﻿using System;
using System.IO;
using System.Windows;
using IzdajaRacunovWPF.Utilities;
using IzdajaRacunovWPF.Utilities.Logging;
namespace IzdajaRacunovWPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        protected override void OnStartup(StartupEventArgs e)
        {
            NLogLogger.ConfigureLogger();


            if (!Directory.Exists(Path.Combine(Directory.GetCurrentDirectory(), "\\lib")))
            {
                Directory.CreateDirectory("lib");
            }

            AppData.Logger = new NLogLogger();
            AppData.Logger.Info("Application starting.");

            // Logging
            Application.Current.DispatcherUnhandledException += (sender, args) =>
            {
                AppData.Logger.Error(args.Exception);
            };

            var window = new MainWindow();

            window.Show();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            AppData.Logger.Info("Application exiting.");

            base.OnExit(e);
        }
    }
    
}
