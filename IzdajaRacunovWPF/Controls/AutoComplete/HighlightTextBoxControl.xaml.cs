﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using IzdajaRacunovWPF.Controls.AutoComplete.Interfaces;
using IzdajaRacunovWPF.Controls.AutoComplete;
using System.Windows.Controls.Primitives;
using System.Windows.Threading;

namespace IzdajaRacunovWPF.Controls.AutoComplete
{
    /// <summary>
    /// Interaction logic for HighlightTextBoxControl.xaml
    /// </summary>
    public partial class HighlightTextBoxControl : AutoCompleteBox
    {

        public HighlightTextBoxControl()
        {
            this.InitializeComponent();
        }

        private int totalCount;
        private SearchByToken searchByToken;
        private Popup _popup;
        private ListBox _listBox;
        

        public void Initialize(SearchByToken searchByToken, Action advancedCommand)
        {
            this.searchByToken = searchByToken;
            this.AdvancedCommand = new RelayCommand(() =>
            {
                advancedCommand.Invoke();
            });
        }

        public delegate List<IViewItem> SearchByToken(string searchToken, out int totalCount);

        private void AutoCompleteBox_Populating(object sender, PopulatingEventArgs e)
        {
            AutoCompleteBox autoComplete = (AutoCompleteBox)sender;

            // Allow us to wait for the response
            e.Cancel = true;

            if (this.searchByToken == null)
                throw new ArgumentException("You must set the SearchByTokenDelegate");

            searchByToken.BeginInvoke(this.highlightTextBox.SearchText, out totalCount, new AsyncCallback(SearchCompleted), searchByToken);
        }

        private void SearchCompleted(IAsyncResult result)
        {
            SearchByToken search = result.AsyncState as SearchByToken;
            List<IViewItem> returnList = search.EndInvoke(out totalCount, result);

            this.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(delegate
            {
                if (returnList == null)
                {
                    this.CurrentCount = 0;
                    this.TotalCount = totalCount;
                    return;
                }
                else
                {
                    this.CurrentCount = returnList.Count();
                    this.TotalCount = totalCount;
                }

                this.highlightTextBox.ItemsSource = returnList;
                this.highlightTextBox.PopulateComplete();
            }));
        }

        private IViewItem _selectedItem;
        public IViewItem SelectedItemManual
        {
            get { return this._selectedItem; }
            set
            {
                if (value != this._selectedItem)
                {
                    this._selectedItem = value;
                }
            }
        }

        private void AutoCompleteBox_Populated(object sender, PopulatedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(delegate
            {
                if (_popup == null)
                {
                    _popup = highlightTextBox
                        .GetLogicalChildrenBreadthFirst()
                        .OfType<Popup>()
                        .FirstOrDefault();
                }
                if (_popup != null)
                {
                    FrameworkElement fe = _popup.Child as FrameworkElement;
                    if (fe != null)
                    {
                        _listBox = fe
                            .GetLogicalChildrenBreadthFirst()
                            .OfType<ListBox>()
                            .FirstOrDefault();
                    }
                }
                if (_listBox != null)
                {
                    foreach (var item in _listBox
                        .GetLogicalChildrenBreadthFirst()
                        .OfType<HighlightingTextBlock>())
                    {
                        item.HighlightText = highlightTextBox.SearchText;
                        this.HighlightSearchText = highlightTextBox.SearchText;
                    }
                }
            }));
        }

        public TextBox FocusBox
        {
            get
            {
                var textBox = highlightTextBox.Template.FindName("Text", highlightTextBox) as TextBox;
                return textBox;
            }
        }

        public RelayCommand AdvancedCommand { get; set; }

        public string HighlightSearchText
        {
            get { return (string)GetValue(HighlightSearchTextProperty); }
            set { SetValue(HighlightSearchTextProperty, value); }
        }

        public static readonly DependencyProperty HighlightSearchTextProperty =
            DependencyProperty.Register("HighlightSearchText", typeof(string), typeof(HighlightTextBoxControl), new UIPropertyMetadata(String.Empty));


        public int TotalCount
        {
            get { return (int)GetValue(TotalCountProperty); }
            set { SetValue(TotalCountProperty, value); }
        }

        public static readonly DependencyProperty TotalCountProperty =
            DependencyProperty.Register("TotalCount", typeof(int), typeof(HighlightTextBoxControl), new UIPropertyMetadata(0));

        public int CurrentCount
        {
            get { return (int)GetValue(CurrentCountProperty); }
            set { SetValue(CurrentCountProperty, value); }
        }

        public static readonly DependencyProperty CurrentCountProperty =
            DependencyProperty.Register("CurrentCount", typeof(int), typeof(HighlightTextBoxControl), new UIPropertyMetadata(0));
    }
}
