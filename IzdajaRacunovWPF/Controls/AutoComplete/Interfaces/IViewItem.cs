﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace IzdajaRacunovWPF.Controls.AutoComplete.Interfaces
{
    public abstract class IViewItem : INotifyPropertyChanged
    {
        public virtual Int32 Id { get; set; }

        public virtual String Title { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(String name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (PropertyChanged != null)
                handler(this, new PropertyChangedEventArgs(name));
        }
    }
}
