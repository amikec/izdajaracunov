﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using IzdajaRacunovWPF.Data;
using IzdajaRacunovWPF.Data.Entity;
using IzdajaRacunovWPF.ViewModel;
using IzdajaRacunovWPF.Controls.AutoComplete;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace IzdajaRacunovWPF.Controls
{
    /// <summary>
    /// Interaction logic for IzdajaRacunovUC.xaml
    /// </summary>
    public partial class IzdajaRacunovUC : UserControl
    {
        public delegate void CloseFormHandler(TabItem uc, EventArgs e);
        public event CloseFormHandler OnFormClose;

        public List<HighlightTextBoxControl> dataGridAutoBoxes;
        public Boolean loaded = true;
        public ViewModel.ViewModel viewModel;
        private String lastInput = String.Empty;


        public IzdajaRacunovUC(BillingForm bill)
        {
            InitializeComponent();

            this.viewModel = new ViewModel.ViewModel();

            if (bill != null)
            {
                dataGrid.Items.Clear();
                loaded = false;
                this.viewModel.Bill = bill;
            }

            this.dataGridAutoBoxes = new List<HighlightTextBoxControl>();
            this.DataContext = this.viewModel;
            this.CustomerAutoComplete.Initialize(viewModel.GetPersonBySearchToken, null);
            this.ProviderAutoComplete.Initialize(viewModel.GetPersonBySearchToken, null);
        }

        public HighlightTextBoxControl CustomerAutoComplete 
        {
            get { return this.searchCustomer; } 
        }

        public HighlightTextBoxControl ProviderAutoComplete
        {
            get { return this.searchProvider; }
        }
        
        /// <summary>
        /// Saves current billing form data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_SaveClick(object sender, RoutedEventArgs e)
        {
            viewModel.Save();
        }

        /// <summary>
        /// Handler for customer autocomplete element, called on text changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoCompleteBox_TextChanged_Customer(object sender, RoutedEventArgs e)
        {
            if (this.viewModel.Bill.Customer == null)
                this.viewModel.Bill.Customer = new Person();

            HighlightTextBoxControl kontrola = (sender as HighlightTextBoxControl);
            Person selectedItem = (sender as HighlightTextBoxControl).SelectedItem as Person;

            if ((selectedItem == null || kontrola.Text.Length != selectedItem.Title.Length) && this.viewModel.Bill.Customer.Id > 0)
                this.viewModel.Bill.Customer = new Person();

            this.viewModel.Bill.Customer.Title = kontrola.Text;
        }

        /// <summary>
        /// Handler for Provider autocomplete element, called on text changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoCompleteBox_TextChanged_Provider(object sender, RoutedEventArgs e)
        {
            if (this.viewModel.Bill.Provider == null)
                this.viewModel.Bill.Provider = new Person();

            HighlightTextBoxControl kontrola = (sender as HighlightTextBoxControl);
            Person selectedItem = (sender as HighlightTextBoxControl).SelectedItem as Person;

            if ((selectedItem == null || kontrola.Text.Length != selectedItem.Title.Length) && this.viewModel.Bill.Provider.Id > 0)
                this.viewModel.Bill.Provider = new Person();

            this.viewModel.Bill.Provider.Title = kontrola.Text;
        }

        /// <summary>
        /// Handler for ItemType autocomplete element, called on text changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoCompleteBox_TextChanged_Type(object sender, RoutedEventArgs e)
        {
            if (!(dataGrid.SelectedItem is ServiceItem))
            {
                if ((sender as HighlightTextBoxControl).Text.Length > 0)
                    lastInput = (sender as HighlightTextBoxControl).Text;

                (sender as HighlightTextBoxControl).Text = String.Empty;

                if (viewModel.Bill.ServiceItems.Count == 0 || !String.IsNullOrEmpty(viewModel.Bill.ServiceItems.Last().Type.Title) && loaded)
                {
                    viewModel.Bill.ServiceItems.Add(new ServiceItem());
                }

                return;
            }

            ServiceItemType selectedItem = (sender as HighlightTextBoxControl).SelectedItem as ServiceItemType;
            ServiceItem selectedRowItem = (ServiceItem)dataGrid.SelectedItem;
            HighlightTextBoxControl kontrola = (sender as HighlightTextBoxControl);

            if (selectedRowItem != null && (kontrola.Text.Length != selectedRowItem.Title.Length) && selectedRowItem.Type.Id > 0 && loaded)
            {
                selectedRowItem.Type = new ServiceItemType();
                selectedRowItem.Price = 0;
                selectedRowItem.Unit = "";
            }

            ((ServiceItem)dataGrid.SelectedItem).Type.Title = kontrola.Text;
        }

        /// <summary>
        /// Handler for provider autocomplete element, called on left mouse button up
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchProvider_MouseLeftButtonUp_Provider(object sender, MouseButtonEventArgs e)
        {
            this.viewModel.Bill.Provider = (sender as HighlightTextBoxControl).SelectedItem as Person;
        }

        /// <summary>
        /// Handler for Customer autocomplete element, called on left mouse button up
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchCustomer_MouseLeftButtonUp_Customer(object sender, MouseButtonEventArgs e)
        {
            this.viewModel.Bill.Customer = (sender as HighlightTextBoxControl).SelectedItem as Person;
        }

        /// <summary>
        /// Handler for ItemType autocomplete element, called on left mouse button up
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void itemTypeSearch_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!(dataGrid.SelectedItem is ServiceItem))
                return;

            HighlightTextBoxControl kontrola = (sender as HighlightTextBoxControl);
            ServiceItem selected = (ServiceItem) dataGrid.SelectedItem;
            ServiceItemType itemType = kontrola.SelectedItem as ServiceItemType;

            if (selected != null && itemType != null)
            {
                selected.Type = itemType;
                selected.Price = itemType.Price;
                selected.Unit = itemType.Unit;
            }
        }

        /// <summary>
        /// Handler for ItemType autocomplete element, executed when the element is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void itemTypeSearch_Loaded_1(object sender, RoutedEventArgs e)
        {
             HighlightTextBoxControl box = sender as HighlightTextBoxControl;
             box.Initialize(viewModel.GetItemTypeBySearchToken, null);

             dataGridAutoBoxes.Add(box);

             if (dataGridAutoBoxes.Count > dataGrid.Items.Count)
             {
                 dataGridAutoBoxes.Clear();
                 dataGridAutoBoxes.Add(box);
                 lastInput = "";
             }

            //when a new autocomplete box is added, we give it focus
             if (viewModel.Bill.ServiceItems.Count > 1)
             {
                 dataGrid.SelectedIndex = dataGrid.Items.Count - 2;

                 if (box.FocusBox.Text.Length == 0 && lastInput.Length > 0 && loaded)
                 {
                     box.Text = lastInput;
                     Keyboard.Focus(box.FocusBox);
                     box.FocusBox.SelectionLength = 0;
                     box.FocusBox.CaretIndex = lastInput.Length;
                 }
             }

             //in case we're opening an existing bill
             if (!loaded && viewModel.Bill.Id > 0 && 
                 viewModel.Bill.ServiceItems.Count + 1 == dataGridAutoBoxes.Count)
             {
                 for (int i = 0; i < viewModel.Bill.ServiceItems.Count; i++)
                 {
                     if (viewModel.Bill.ServiceItems[i].Id > 0)
                     {
                         ServiceItemType itemType = viewModel.Bill.ServiceItems[i].Type;
                         dataGrid.SelectedIndex = i;
                         dataGridAutoBoxes[i].SelectedItem = itemType;
                         (dataGrid.SelectedItem as ServiceItem).Price = itemType.Price;
                         (dataGrid.SelectedItem as ServiceItem).Unit = itemType.Unit;
                     }
                 }

                 loaded = true;
             }
        }

        private void DataGridTextColumn_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            this.viewModel.Bill.Price = 0;

            foreach(ServiceItem item in this.viewModel.Bill.ServiceItems)
            {
                this.viewModel.Bill.Price += item.Price * item.Quantity;
            }
        }

        private void Button_CloseClick(object sender, RoutedEventArgs e)
        {
            if (!this.viewModel.Bill.IsClosable())
            {
                MessageBoxResult result = MessageBox.Show("Račun še ni shranjen, ali vseeno želite zapreti račun?", "Shrani", MessageBoxButton.YesNo);

                if (result == MessageBoxResult.No)
                    return;       
            }

            OnFormClose(this.Parent as TabItem, new EventArgs());
        }
    }
}
