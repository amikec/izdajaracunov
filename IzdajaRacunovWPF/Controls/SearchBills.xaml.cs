﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading.Tasks;
using IzdajaRacunovWPF.Data.Entity;
using IzdajaRacunovWPF.Controls.AutoComplete;
using IzdajaRacunovWPF.Utilities;
using IzdajaRacunovWPF.Controls.AutoComplete.Interfaces;

namespace IzdajaRacunovWPF.Controls
{
    /// <summary>
    /// Interaction logic for SearchBills.xaml
    /// </summary>
    public partial class SearchBills : UserControl
    {
        public delegate void RacunFindDelegate(SearchBills uc, BillingForm bill, EventArgs e);
        public event RacunFindDelegate OnRacunFind;
        private Boolean isProcessing;

        public SearchBills()
        {
            InitializeComponent();
            AppData.RefreshBillsData();
            this.DataContext = AppData.Bills;
            this.searchBills.Initialize(SearchByToken, null);
        }

        public List<IViewItem> SearchByToken(string searchToken, out int count)
        {
            if (!isProcessing)
            {
                isProcessing = true;

                var result = AppData.Bills.AsEnumerable().Where(n => n.Customer.Title.ToLower().Split(" ".ToCharArray()).Any(m => m.Length > 0 && searchToken.ToLower().Split(" ".ToCharArray()).Any(k => k.Length > 0 && m.StartsWith(k)))).ToList<IViewItem>();
                count = result.Count();

                isProcessing = false;

                return result;
            }

            count = 0;
            return null;
        }

        private void searchBills_MouseLeftButtonUp_1(object sender, MouseButtonEventArgs e)
        {
            BillingForm bill = (sender as HighlightTextBoxControl).SelectedItem as BillingForm;
            OnRacunFind(this, bill, new EventArgs());
        }
    }
}
