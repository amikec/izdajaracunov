﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using IzdajaRacunovWPF.Controls.AutoComplete;
using IzdajaRacunovWPF.Data.Entity;
using IzdajaRacunovWPF.ViewModel;

namespace IzdajaRacunovWPF.Controls
{
    /// <summary>
    /// Interaction logic for ManageMaterial.xaml
    /// </summary>
    public partial class ManageMaterial : UserControl
    {
        public List<HighlightTextBoxControl> dataGridAutoBoxes = new List<HighlightTextBoxControl>();
        public delegate void CloseFormHandler(TabItem uc, EventArgs e);
        public event CloseFormHandler OnFormClose;
        private String lastInput = String.Empty;

        public ManageMaterial()
        {
            InitializeComponent();
            this.ViewModel = new ViewModelMaterial();            
        }

        private void AutoCompleteBox_TextChanged_Type(object sender, RoutedEventArgs e)
        {
            // add a new row
            if (!(dataGrid.SelectedItem is ServiceItemType))
            {
                if((sender as HighlightTextBoxControl).Text.Length > 0)
                    lastInput = (sender as HighlightTextBoxControl).Text;

                (sender as HighlightTextBoxControl).Text = String.Empty;

                if (ViewModel.CurrentItemTypes.Count == 0 || !String.IsNullOrEmpty(ViewModel.CurrentItemTypes.Last().Title))
                {
                    ViewModel.CurrentItemTypes.Add(new ServiceItemType());
                }

                return;
            }

            // set item data
            ServiceItemType selectedItem = (sender as HighlightTextBoxControl).SelectedItem as ServiceItemType;
            ServiceItemType selectedRowItem = (ServiceItemType)dataGrid.SelectedItem;
            HighlightTextBoxControl kontrola = (sender as HighlightTextBoxControl);

            if ((kontrola.Text.Length != selectedRowItem.Title.Length) && selectedRowItem.Id > 0)
            {
                selectedRowItem.Title = "";
                selectedRowItem.Price = 0;
                selectedRowItem.Unit = "";
                selectedRowItem.Id = -1;
            }

            ((ServiceItemType)dataGrid.SelectedItem).Title = kontrola.Text;
        }

        public ViewModelMaterial ViewModel
        {
            get { return this.DataContext as ViewModelMaterial; }
            set { this.DataContext = value; }
        }

        /// <summary>
        /// Handler for AutoComplete MousLeftButtonUp event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchItemTypes_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!(dataGrid.SelectedItem is ServiceItemType))
                return;

            HighlightTextBoxControl kontrola = (sender as HighlightTextBoxControl);
            ServiceItemType selectedRowItem = (ServiceItemType)dataGrid.SelectedItem;

            ServiceItemType itemType = kontrola.SelectedItem as ServiceItemType;

            if (selectedRowItem != null && itemType != null)
            {
                selectedRowItem.Id = itemType.Id;
                selectedRowItem.Title = itemType.Title;
                selectedRowItem.Unit = itemType.Unit;
                selectedRowItem.Price = itemType.Price;
            }
        }

        /// <summary>
        /// Handler for Autocomplete Loaded event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchMaterial_Loaded_1(object sender, RoutedEventArgs e)
        {
            HighlightTextBoxControl box = sender as HighlightTextBoxControl;
            box.Initialize(ViewModel.GetItemTypeBySearchToken, null);

            dataGridAutoBoxes.Add(box);

            if (dataGridAutoBoxes.Count > dataGrid.Items.Count)
            {
                dataGridAutoBoxes.Clear();
                dataGridAutoBoxes.Add(box);
                lastInput = "";
            }

            //we have to change focus and set the caret position           
            if (box.FocusBox.Text.Length == 0 && lastInput.Length > 0)
            {
                dataGrid.SelectedIndex = dataGrid.Items.Count - 2;
                box.Text = lastInput;
                Keyboard.Focus(box.FocusBox);
                box.FocusBox.SelectionLength = 0;
                box.FocusBox.CaretIndex = lastInput.Length;
            }
        }

        private void Button_Click_Close(object sender, RoutedEventArgs e)
        {
            foreach (ServiceItemType type in ViewModel.CurrentItemTypes)
            {
                if (type.Id < 1)
                {
                    System.Windows.MessageBoxResult result = System.Windows.MessageBox.Show("Vse spremembe niso shranjene, želite vseeno zapreti zavihek?", "Obvestilo", MessageBoxButton.YesNo);
                    if (result == System.Windows.MessageBoxResult.No)
                        return;
                }
            }

            OnFormClose(this.Parent as TabItem, new EventArgs());
        }

        private void Button_Click_Save(object sender, RoutedEventArgs e)
        {
            ViewModel.Save();
        }

        /// <summary>
        /// Marks which objects we want to delete from the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuItem_Click_Delete(object sender, RoutedEventArgs e)
        {
            ServiceItemType item = (this.dataGrid.SelectedItem as ServiceItemType);

            if(item.Id > 0)
                ViewModel.ToBeDeleted.Add(item);
        }
    }
}
