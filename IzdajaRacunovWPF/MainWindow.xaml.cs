﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using IzdajaRacunovWPF.Data;
using IzdajaRacunovWPF.Data.Entity;
using IzdajaRacunovWPF.ViewModel;
using IzdajaRacunovWPF.Controls;
using System.ComponentModel;
using System.Collections.ObjectModel;
using IzdajaRacunovWPF.Utilities;
using IzdajaRacunovWPF.Controls.AutoComplete;

namespace IzdajaRacunovWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            AppData.Logger.Info("Initializing MainWindow");

            InitializeComponent();
            AppData.Init();
            AppData.RefreshData();
            AddNewTabItem();
        }

        private void AddNewTabItem()
        {
            TabItem tab = new TabItem();
            tab.Header = String.Format("Račun {0}", this.tabControl.Items.Count + 1);

            IzdajaRacunovUC uc = new IzdajaRacunovUC(null);
            tab.Content = uc;
            uc.OnFormClose += uc_OnFormClose;

            this.tabControl.Items.Add(tab);
            tab.IsSelected = true;
        }

        /// <summary>
        /// Handler for onFormClose event
        /// </summary>
        /// <param name="uc"></param>
        /// <param name="e"></param>
        void uc_OnFormClose(TabItem uc, EventArgs e)
        {
            this.tabControl.Items.Remove(uc);
        }

        private void newMenuItem_Click(object sender, RoutedEventArgs e)
        {
            AddNewTabItem();
        }

        private void exitMenuItem_Click(object sender, RoutedEventArgs e)
        {
            foreach (TabItem item in this.tabControl.Items)
            {
                if (!(item.Content is IzdajaRacunovUC))
                    continue;

                if (!(item.Content as IzdajaRacunovUC).viewModel.Bill.IsClosable())
                {
                    MessageBoxResult result = MessageBox.Show("Vsi računi niso shranjeni, ali vseeno želite zapreti aplikacijo?", "Shrani", MessageBoxButton.YesNo);

                    if (result == MessageBoxResult.No)
                        return;

                    break;
                }
            }
            
            this.Close();
        }

        /// <summary>
        /// Handler for OnRacunFind event
        /// </summary>
        /// <param name="uc"></param>
        /// <param name="bill"></param>
        /// <param name="e"></param>
        void uc_OnRacunFind(SearchBills uc, BillingForm bill, EventArgs e)
        {
            if (bill == null)
                return;

            TabItem tab = uc.Parent as TabItem;
            String tip = bill.Informational ? "Predračun" : "Račun";
            tab.Header = String.Format("{0} {1}",tip, bill.Code);

            IzdajaRacunovUC uc2 = new IzdajaRacunovUC(bill);

            uc2.searchProvider.SelectedItem = bill.Provider;
            uc2.searchCustomer.SelectedItem = bill.Customer;

            tab.Content = uc2;
            uc2.OnFormClose += uc_OnFormClose;
            tab.IsSelected = true;
        }

        private void openMenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            SearchBills uc = new SearchBills();
            TabItem item = new TabItem();
            uc.OnRacunFind += uc_OnRacunFind;
            item.Header = "Odpri račun ..";
            item.Content = uc;
            tabControl.Items.Add(item);
            item.IsSelected = true;
        }

        private void editMaterialMenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            ManageMaterial uc = new ManageMaterial();
            uc.OnFormClose += uc_OnFormClose;
            TabItem tab = new TabItem();
            tab.Header = "Urejaj material";
            tab.Content = uc;
            tabControl.Items.Add(tab);
            tab.IsSelected = true;
        }
    }
}
